__author__ = 'Igor Olshevsky'

from math import *

# som
class PyMathParser(object):
	"""
	Mathematical Expression Evaluator class.
	You can set the expression member, set the functions, variables and then call
	evaluate() function that will return you the result of the mathematical expression
	given as a string.
	"""

	"""
	Mathematical expression to evaluate.
	"""
	expression = ''

	"""
	Dictionary of functions that can be used in the expression.
	"""
	functions = {'__builtins__': None}

	"""
	Dictionary of variables that can be used in the expression.
	"""
	variables = {'__builtins__': None}

	def __init__(self):
		"""
		Constructor
		"""

	def evaluate(self):
		"""
		Evaluate the mathematical expression given as a string in the expression member variable.
		"""
		return eval(self.expression, self.variables, self.functions)

	def add_default_functions(self):
		"""
		Add the following Python functions to be used in a mathemtical expression:
		"""
		self.functions['acos'] = acos
		self.functions['asin'] = asin
		self.functions['atan'] = atan
		self.functions['atan2'] = atan2
		self.functions['ceil'] = ceil
		self.functions['cos'] = cos
		self.functions['cosh'] = cosh
		self.functions['degrees'] = degrees
		self.functions['exp'] = exp
		self.functions['fabs'] = fabs
		self.functions['floor'] = floor
		self.functions['fmod'] = fmod
		self.functions['frexp'] = frexp
		self.functions['hypot'] = hypot
		self.functions['ldexp'] = ldexp
		self.functions['log'] = log
		self.functions['log10'] = log10
		self.functions['modf'] = modf
		self.functions['pow'] = pow
		self.functions['radians'] = radians
		self.functions['sin'] = sin
		self.functions['sinh'] = sinh
		self.functions['sqrt'] = sqrt
		self.functions['tan'] = tan
		self.functions['tanh'] = tanh

	def add_default_variables(self):
		"""
		Add e and pi to the list of defined variables.
		"""
		self.variables['e'] = e
		self.variables['pi'] = pi

	def add_var(self, text):
		"""
		add custom var to list
			syntax: <^variable name>=<variable value> i.e. "x=0"
		IMPORTANT!  If parent var was changed - child(ren) vars will not change
		"""
		name = text.split("=")[0]
		means = eval(text.split("=")[1], self.variables, self.functions)
		self.variables[name] = means

	def add_function(self, text):
		"""
		add custom function to list
			syntax: <function name>(<parameter(s)>)=<function body (variables with operators)>
		"""
		step_1 = text.split("=")
		step_2 = step_1[0].split("(")
		fx_name = step_2[0]
		exec("def " + step_1[0] + ": return " + step_1[1]) in locals()
		fx_body = locals()[fx_name]
		self.functions[fx_name] = fx_body
		globals().update(locals())

	def get_variable_names(self):
		"""
		Return a List of defined variables names in sorted order.
		"""
		my_list = list(self.variables.keys())
		try:
			my_list.remove('__builtins__')
		except ValueError:
			pass
		my_list.sort()
		return my_list

	def get_functions_names(self):
		"""
		Return a List of defined function names in sorted order.
		"""
		my_list = list(self.functions.keys())
		try:
			my_list.remove('__builtins__')
		except ValueError:
			pass
		my_list.sort()
		return my_list

	def init(self):
		self.add_default_functions()
		self.add_default_variables()


'''init section'''
instance = PyMathParser()
instance.init()
