.. _top:

Welcome to NumIntComparator - Numerical integration methods comparator
======================================================================
**Short overview**

Software written on Python programming language, and based on Kivy as GUI engine

.. note::
	There are embedded images. Be careful!

.. note::
	This document can be dispalyed incorrectly. To avoid this proceed to the
	http://bitbucket.org/oligval/pyintintegr

.. image:: http://i.imgur.com/ROoyt7T.png

Return to top_
	
List of available methods
=========================
	
It can integrate through this list of methods:
	
- Rectangles Method
- Trapezium Method
- Simpson's Method
- Monte Carlo Method
- Gauss 10-point method
- Chebishev Method
- Spline method

By default, selected method is *Rectangles Method*, but you can select **all** them!

Return to top_

GUI overview 
============

On the first screenshot You can some buttons:

Menu section

- **Methods** - methods select pop-up, just like as second screen 
- **Info** - *this* window
- **Variables** list - pop-up with defined variables
- **Functions list** - pop-up with defined functions
- **Clear** - Clears *input* and *output* areas
- **Evaluate** - calculate everyting, that You print in input area

Return to top_

Input syntax
============

There are 4 types of input:

- Variable input
- Function input
- Integrand input
- Just evaluate input

Return to top_

Syntax of variable input
------------------------

*	``^x=10`` - Creates a variable with name ``x`` and contains a value of *10*

*	``^y=x+2`` - If variable ``x`` exists - creates a new variable ``y`` with some changes, based on ``x`` (adds 2 to ``x`` in this case)

**To check if variables exists and are correct - just press the 'Variable list' button**

.. Note::
	Variables are using Python syntax: ' ** ' instead of ' ^ ' , ' // ' for using integer (floor) division*

.. image:: http://i.imgur.com/1M9M229.png

Return to top_

Syntax of functions input
-------------------------

* ``:fx(x)=x+10`` - Creates a function with name ``fx`` that returns ``value+10``

* ``:fy(x)=fx(x)+e**x`` - Functions can be more complex - based on other functions. 

**To check if functions exists - just press the 'Functions list' button**

.. Note::
	Functions respectively to the varibles are using Python syntax: ' ** ' instead of ' ^ ' , ' // ' for using integer (floor) division*


.. image:: http://i.imgur.com/WrPRA1Q.png

Return to top_

Syntax of integrand input
-------------------------

* ``#sin:x=[1,0]`` - Integrate ``sin`` function from ``1`` to ``0``

* ``#awesomeFunction:x=[a,b]`` - Integrate ``awesomeFunction`` function from ``a`` to ``b`` - if ``awesomeFunction``, ``a`` and ``b`` are defined in theirs lists.
	


.. Note::

	In other case, to maintain stability of software:

	If ``awesomeFunction`` is not defined - it will be replaced with ``e**x``;

	If ``a`` **or** ``b`` is not defined - then they will be replaced with ``0``;

	If ``a`` **and** ``b`` is not defined - then they will be replaced with ``0`` and ``1``, respectively;

Return to top_

Syntax of evaluation input
--------------------------

There are no special prefixes:

* ``1+2`` - without any prefixes will return ``3``

Also, You can use defined functions and variables in evaluation

* ``sin(pi+0.5)`` - will return ``-0.4794255386042029``

Return to top_