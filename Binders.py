__author__ = 'Igor Olshevsky'

import time

import MathParser
import Methods
import Utils

m_parser = MathParser.instance
meth_list = Methods.wrapper
l = [True, False, False, False, False, False, False]
l_names = []
l_names.append("Method of Rectangles")
l_names.append("Trapezium Method")
l_names.append("Simpsons Method")
l_names.append("Monte-Carlo Method")
l_names.append("Gauss 10 points method")
l_names.append("Chebishev method")
l_names.append("Spline method")


def clear(self):
	self.ids["input"].text = ""
	self.ids["comp_value"].text = 'Output\n'
	self.ids["comp_value"].height = self.ids["out_scroll"].height * 2


def get_var_callback(self):
	var_names = m_parser.get_variable_names()
	for i in range(len(var_names)):
		self.ids["v_list"].text += var_names[i] + '=' + str(m_parser.variables[var_names[i]]) +'\n'


def get_fx_callback(self):
	fx_names = m_parser.get_functions_names()
	for i in range(len(fx_names)):
		self.ids["fx_list"].text += fx_names[i] + '\n'


def eval_callback(self):
	start_time = time.time()
	target = self.ids["input"].text
	output = self.ids["comp_value"]
	output.text += '\n'
	if target != '':
		global l
		if target[0] == '#':
			smth = Utils.clarify(target)
			output.text += Utils.format_out(target)
			for i in range(0, len(l), 1):
				if l[i]:
					output.text += l_names[i] + ' ' + "{:.12f}".format((meth_list[i](smth[0], smth[1], smth[2]))) + '\n'
					output.height = self.ids["out_scroll"].height * 2 + 1
		else:
			output.text += str(Utils.clarify(target))
	end_time = time.time()
	time_res = "{:.3f} sec".format(end_time - start_time)
	self.ids["Time"].text = time_res


def methods_callback(self):
	m = []
	for i in range(0, 14, 2):
		m.append(self.ids["methods"].children[i].active)
	global l
	l = m[::-1]
