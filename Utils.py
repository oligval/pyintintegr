__author__ = 'Igor Olshevsky'
import MathParser

m_parser = MathParser.instance


def setf(target):
	if target in m_parser.functions:
		return m_parser.functions[target]
	elif not hasattr(target, '__call__'):
		print('No ' + target + ' in functions list. Exp used')
		return m_parser.functions['exp']
	else:
		return target


def setv(target):
	try:
		return float(target)
	except ValueError:
		if target in m_parser.variables:
			return m_parser.variables[target]
		else:
			print('No target v in list. 0 used')
			return 0


def frange(start, end=None, inc=None):
	"""A range function, that does accept float increments..."""

	if end is None:
		end = start + 0.0
		start = 0.0

	if inc is None:
		inc = 1.0

	result = []
	while 1:
		curr = start + len(result) * inc
		if inc > 0 and curr >= end:
			break
		elif inc < 0 and curr <= end:
			break
		result.append(curr)

	return result


def eval_parser(data):
	"""

	sin(x):x=[-1.0, 1.0]

	or

	sin(x):x=[a, b] if `a` and `b` variables are in list

	"""

	p1 = data.split(':')[0]
	p5 = p1
	p1 = p1.split('(')[0][0:]
	p1 = setf(p1)
	p2 = data.split('[')[1].split(',')
	p3 = p2[0].split(']')[0]
	p3 = setv(p3)
	p4 = p2[1].split(',')[0]
	p4 = p4[:-1]
	p4 = setv(p4)
	return p1, p3, p4, p5


text = []
text.append("^x=1")
text.append("^y=a")
text.append(":fx(x)=x+1")
text.append(":fy(x):z")
text.append("#fx:x=[0,1]")
text.append("#sin:x=[0,1]")
text.append("2*pi")
text.append("a=b")


def w2use(data):
	p1 = data[0]
	if p1 == '^':
		return 1
	elif p1 == ':':
		return 2
	elif p1 == '#':
		return 3
	else:
		return 4


def clarify(data):
	p1 = w2use(data)
	if p1 == 1:
		try:
			p2 = data[1:]
			m_parser.add_var(p2)
			return "Success in Var"
		except NameError:
			return "Fail in Var"
	elif p1 == 2:
		try:
			p2 = data[1:]
			m_parser.add_function(p2)
			return "Success in Func"
		except (NameError, SyntaxError):
			return "Fail in Func"
	elif p1 == 3:
		try:
			p2 = data[1:]
			return eval_parser(p2)
		except (NameError, SyntaxError, TypeError, IndexError):
			return "Fail in Integr"
	else:
		try:
			m_parser.expression = data
			return m_parser.evaluate()
		except (NameError, SyntaxError, TypeError, IndexError):
			return "Fail in Eval"


def format_input(func, min_lim, max_lim):
	fx = setf(func)
	min_lim = setv(min_lim)
	max_lim = setv(max_lim)
	if min_lim == max_lim:
		max_lim += 1
	return fx, min_lim, max_lim


def format_out(data):
	t_str1 = eval_parser(data)[-1][1:]
	if t_str1 not in m_parser.functions:
		t_str1 = 'e**x'
	t_str2 = str(eval_parser(data)[1])
	t_str3 = str(eval_parser(data)[2])
	t_out = '\n' + t_str1 + ' From ' + t_str2 + ' To ' + t_str3 + '\n'
	return t_out
