__author__ = 'Igor Olshevsky'
import kivy

import Binders

kivy.require('1.8.0')
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout


class RootWidget(BoxLayout):
	def __init__(self, **kwargs):
		super(RootWidget, self).__init__(**kwargs)

	eval_callback = Binders.eval_callback
	get_var_callback = Binders.get_var_callback
	get_fx_callback = Binders.get_fx_callback
	clear = Binders.clear
	methods_callback = Binders.methods_callback


class NumComparator(App):
	def build(self):
		return RootWidget()


if __name__ == '__main__':
	NumComparator().run()
