__author__ = 'Igor Olshevsky'

import math
import random

import MathParser
import Utils

m_parser = MathParser.instance


def method_of_rectangles(func, min_lim=0, max_lim=1, delta=0.01):
	fx, min_lim, max_lim = Utils.format_input(func, min_lim, max_lim)

	def integrate(range_x):
		integral = 0.0
		step = (max_lim - min_lim) / range_x
		for x in Utils.frange(min_lim, max_lim - step, step):
			integral += step * fx(x + step / 2)
		return integral

	d, n = 1, 1
	while math.fabs(d) > delta:
		d = (integrate(n * 2) - integrate(n)) / 3
		n *= 2

	return integrate(n) + d


def trapezium_method(func, min_lim, max_lim, delta=0.01):
	fx, min_lim, max_lim = Utils.format_input(func, min_lim, max_lim)

	def integrate(range_x):
		integral = 0.0
		step = (max_lim - min_lim) / range_x
		for x in Utils.frange(min_lim, max_lim - step, step):
			integral += step * (fx(x) + fx(x + step)) / 2
		return integral

	d, n = 1, 1
	while math.fabs(d) > delta:
		d = (integrate(n * 2) - integrate(n)) / 3
		n *= 2

	return integrate(n) + d


def simpson_method(func, min_lim, max_lim, delta=0.01):
	fx, min_lim, max_lim = Utils.format_input(func, min_lim, max_lim)

	def integrate(range_x):
		integral = 0.0
		step = (max_lim - min_lim) / range_x
		for x in Utils.frange(min_lim + step / 2, max_lim - step / 2, step):
			integral += step / 6 * (fx(x - step / 2) + 4 * fx(x) + fx(x + step / 2))
		return integral

	d, n = 1, 1
	while math.fabs(d) > delta:
		d = (integrate(n * 2) - integrate(n)) / 15
		n *= 2

	return integrate(n) + d


def gauss_10(func, min_lim, max_lim, delta=0.01):
	fx, min_lim, max_lim = Utils.format_input(func, min_lim, max_lim)

	def gauss_calc(a, b):
		g10c1 = 0.9739065285 / 6.2012983932
		g10c2 = 0.8650633667 / 6.2012983932
		g10c3 = 0.6794095683 / 6.2012983932
		g10c4 = 0.4333953941 / 6.2012983932
		g10c5 = 0.1488743390 / 6.2012983932
		g10x1 = 0.0666713443 / 6.2012983932
		g10x2 = 0.1494513492 / 6.2012983932
		g10x3 = 0.2190863625 / 6.2012983932
		g10x4 = 0.2692667193 / 6.2012983932
		g10x5 = 0.2955242247 / 6.2012983932
		m = (b + a) / 2.0
		n = (b - a) / 2.0
		s1 = g10c1 * (fx(m + n * g10x1) + fx(m - n * g10x1))
		s2 = g10c2 * (fx(m + n * g10x2) + fx(m - n * g10x2))
		s3 = g10c3 * (fx(m + n * g10x3) + fx(m - n * g10x3))
		s4 = g10c4 * (fx(m + n * g10x4) + fx(m - n * g10x4))
		s5 = g10c5 * (fx(m + n * g10x5) + fx(m - n * g10x5))
		s = s1 + s2 + s3 + s4 + s5
		return s * (b - a)

	def gauss(a, b, eps, gc):
		t = (a + b) / 2.0
		ga = gauss_calc(a, t)
		gb = gauss_calc(t, b)
		if math.fabs(ga + gb - gc) > eps:
			eps /= 2
			ga = gauss(a, t, eps, ga)
			gb = gauss(t, b, eps, gb)
		return ga + gb

	return gauss(min_lim, max_lim, delta, gauss_calc(min_lim, max_lim))


def cheb_method(func, min_lim, max_lim):
	fx, min_lim, max_lim = Utils.format_input(func, min_lim, max_lim)
	n = 5
	k = -0.832498
	l = -0.374541
	z = 0.0
	h = (max_lim - min_lim) / n
	ich = 0.0
	x, y, t = [], [], []
	t = [k, l, z, l, k]
	for i in range(0, n - 2):
		x.append(((max_lim + min_lim) / 2 + (max_lim - min_lim) / 2 * t[i]))
	for i in range(n - 1, n + 1):
		x.append(1 - x[n - i])
	for i in range(0, n):
		y.append(fx(x[i]))
	for i in range(0, n):
		ich += y[i] * h
	return ich


def mc_method(func, min_lim=0, max_lim=1, n=100):
	fx, min_lim, max_lim = Utils.format_input(func, min_lim, max_lim)
	s = 0
	for i in range(n):
		x = random.uniform(min_lim, max_lim)
		s += fx(x)
	out = (float(max_lim - min_lim) / n) * s
	return out


def spline_method(func, min_lim, max_lim):
	fx, min_lim, max_lim = Utils.format_input(func, min_lim, max_lim)
	total = 1000
	n = total - 1
	f = []
	c = []
	alpha = []
	beta = []
	tau = (max_lim - min_lim) / total
	tmp = 0

	for i in range(0, n + 1):
		c.append(0.0)
		alpha.append(0.0)
		beta.append(0.0)

	for i in range(0, total + 1):
		x = min_lim + tau * i
		f.append(fx(x))

	alpha[0] = (-1 / 4.0)
	beta[0] = (f[2] - 2.0 * f[1] + f[0])

	for i in range(1, n - 1):
		alpha[i] = -1.0 / (alpha[i - 1] + 4)
		beta[i] = (f[i + 2] - 2 * f[i + 1] + f[i] - beta[i - 1]) / (alpha[i - 1] + 4)

	c[n - 1] = (f[total] - 2 * f[total - 1] + f[total - 2] - beta[n - 1]) / (4 + alpha[n - 1])
	for i in range(n - 2, 0, -1):
		c[i] = alpha[i + 1] * c[i + 1] + beta[i + 1]
	for i in range(0, n):
		c[i] = c[i] * 3 / (tau * tau)
	x = (5 * f[0] + 13 * f[1] + 13 * f[total - 1] + 5 * f[total]) / 12
	for i in range(2, total - 1):
		tmp += f[i]
	x = (x + tmp) * tau - (c[0] + c[n - 1]) * tau * tau * tau / 36

	return x


def diff(data, min_lim, max_lim):
	return str(math.fabs(data - (math.e ** max_lim - math.e ** min_lim)))


def testf(x):
	return math.e ** x


def test(fx, min_lim, max_lim, delta=0.01):
	t1 = method_of_rectangles(fx, min_lim, max_lim, delta)
	t2 = trapezium_method(fx, min_lim, max_lim, delta)
	t3 = simpson_method(fx, min_lim, max_lim, delta)
	t4 = mc_method(fx, min_lim, max_lim)
	t5 = gauss_10(fx, min_lim, max_lim, delta)
	t6 = cheb_method(fx, min_lim, max_lim)
	t7 = spline_method(fx, min_lim, max_lim)
	out1 = "Method of Rectangles\n" + str(t1) + '\t' + 'Diff=' + diff(t1, min_lim, max_lim)
	out2 = "Trapezium Method\n" + str(t2) + '\t' + 'Diff=' + diff(t2, min_lim, max_lim)
	out3 = "Simpsons Method\n" + str(t2) + '\t' + 'Diff=' + diff(t3, min_lim, max_lim)
	out4 = "Monte-Carlo Method\n" + str(t4) + '\t' + 'Diff=' + diff(t4, min_lim, max_lim)
	out5 = "Gauss 10 points method\n" + str(t5) + '\t' + 'Diff=' + diff(t5, min_lim, max_lim)
	out6 = "Chebishev quadrature method\n" + str(t6) + '\t' + 'Diff=' + diff(t6, min_lim, max_lim)
	out7 = "Spline method\n" + str(t7) + '\t' + 'Diff=' + diff(t7, min_lim, max_lim)
	print(out1)
	print(out2)
	print(out3)
	print(out4)
	print(out5)
	print(out6)
	print(out7)

# print('Test1\n')
# test(testf, 0.0, 1.0, 0.01)
# print('Test2\n')
# test(testf, -2.0, 2.05, 0.3)

wrapper = [method_of_rectangles, trapezium_method, simpson_method, mc_method, gauss_10, cheb_method, spline_method]
