:: Create a folder with a source for Kivy Launcher
mkdir ..\numcomparator
copy android.txt ..\numcomparator\android.txt
copy binders.py ..\numcomparator\binders.py
copy MathParser.py ..\numcomparator\MathParser.py
copy Methods.py ..\numcomparator\Methods.py
copy NumComparator.kv ..\numcomparator\NumComparator.kv
copy main.py ..\numcomparator\main.py
copy NumComparator.kv ..\numcomparator\NumComparator.kv
copy Utils.py ..\numcomparator\Utils.py
copy README.rst ..\numcomparator\README.rst